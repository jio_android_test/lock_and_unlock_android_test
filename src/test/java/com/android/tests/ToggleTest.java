package com.android.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.android.utils.AndroidUtils;
import com.android.utils.MobileDevice;

public class ToggleTest {
	private AndroidUtils android;

	@BeforeClass(description = "Initializing the Device Local or Remote ", enabled = true)
	public void setTheDevice() {
		android = new AndroidUtils(MobileDevice.LOCAL);
		//LOCAL- Local Machine
		//BS - Browerstack
	}

	@Test(description = "Toggle Test for Lock and Unlock Device ", priority = 1)
	public void verifyLockandUnlockDevice() {
		//15 - Time delay after lock the device
		//2 - Time delay after unlock the device
		android.toggleLockAndUnlock(15, 2);
	}
}
