package com.android.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.testUtils.TestUtils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class AndroidUtils {

	private static String userName = TestUtils.getPropertiesForAndroid("USERNAME");
	private static String accessKey = TestUtils.getPropertiesForAndroid("KEY");
	private AndroidDriver<AndroidElement> driver;
	private Wait<WebDriver> wait;
	public AndroidUtils(MobileDevice e) {
		if (e == MobileDevice.LOCAL) {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("platformName", "Android");
			caps.setCapability("appium:deviceName", "emulator-5554");
			try {
				driver = new AndroidDriver<AndroidElement>(new URL("http://localhost:4723/wd/hub"), caps);
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		else if (e == MobileDevice.BS) {
			DesiredCapabilities caps = new DesiredCapabilities();

			caps.setCapability("device", TestUtils.getPropertiesForAndroid("DEVICE"));
			caps.setCapability("os_version", TestUtils.getPropertiesForAndroid("OS_VERSION"));
			caps.setCapability("project", TestUtils.getPropertiesForAndroid("PROJECT"));
			caps.setCapability("build", TestUtils.getPropertiesForAndroid("BUILD"));
			caps.setCapability("name", TestUtils.getPropertiesForAndroid("NAME"));

			try {
				driver = new AndroidDriver<AndroidElement>(
						new URL("https://" + userName + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub"), caps);
			} catch (MalformedURLException exception) {
				// TODO Auto-generated catch block
				exception.printStackTrace();
			}
		}
		wait = new WebDriverWait(driver, 30);
	}

	public void enterText(By locator, String text) {
		AndroidElement element = (AndroidElement) wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		element.sendKeys(text);
	}

	public void clickOn(By locator) {
		AndroidElement element = (AndroidElement) wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		element.click();
	}
	public void lockPhoneFor(int time) {
			driver.lockDevice(Duration.ofSeconds(time));
	}
	public void unlockPhone() {
		driver.unlockDevice();
	}
	public void pauseFor(int time) {
		try {
			Thread.sleep(time*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void toggleLockAndUnlock(int timeInSec, int pollTimeInSec) {
		while(true) {
			if(driver.isDeviceLocked()) {
				unlockPhone();
				lockPhoneFor(timeInSec);
				unlockPhone();
				pauseFor(pollTimeInSec);
			} else {
				lockPhoneFor(timeInSec);
				unlockPhone();
				pauseFor(pollTimeInSec);
			}
		}
	}
		
	}

