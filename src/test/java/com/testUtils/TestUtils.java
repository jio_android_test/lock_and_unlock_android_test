package com.testUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

public final class TestUtils {

	public static String getPropertiesForAndroid(String keyName) {
		File myFile = new File(System.getProperty("user.dir") + "//config//" + "android-device.properties");
		Reader reader = null;
		try {
			reader = new FileReader(myFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Properties prop = new Properties();
		try {
			prop.load(reader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String data = prop.getProperty(keyName);
		return data;
	}
}
